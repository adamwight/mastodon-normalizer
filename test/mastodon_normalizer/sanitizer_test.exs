defmodule MastodonNormalizer.HtmlSanitizerTest do
  use ExUnit.Case, async: true
  alias MastodonNormalizer.StatusSanitizer

  test "don't strip links" do
    html = ~s(<a href="http://blar.com/">foo</a>)
    assert html == scrub(html)
  end

  test "strip link target" do
    html = ~s(<a href="https://a.test" target="_blank">foo</a>)
    assert ~s(<a href="https://a.test">foo</a>) == scrub(html)
  end

  def scrub(input) do
    HtmlSanitizeEx.Scrubber.scrub(input, StatusSanitizer)
  end
end
