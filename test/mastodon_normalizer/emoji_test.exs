defmodule MastodonNormalizer.EmojiTest do
  use ExUnit.Case, async: true

  alias MastodonNormalizer.Emoji

  test "decodes shortcodes in text" do
    assert "😀" == Emoji.replace_shortcodes(":grinning:")
    assert "a😀b" == Emoji.replace_shortcodes("a:grinning:b")
    assert "👩🏻‍🤝‍👩🏽" == Emoji.replace_shortcodes(":holding_hands_ww_tone1-3:")
  end

  test "fun shortcodes not yet supported" do
    assert ":neocat_box:" == Emoji.replace_shortcodes(":neocat_box:")
    assert ":Blobhaj_Ghostie_Surprise:" == Emoji.replace_shortcodes(":Blobhaj_Ghostie_Surprise:")
  end
end
