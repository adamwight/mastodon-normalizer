defmodule MastodonNormalizer.MastodonApiTest do
  use ExUnit.Case
  alias MastodonNormalizer.MastodonApi

  @raw_post %{
    "account" => %{
      "acct" => "testy@mastodon.test",
      "avatar" =>
        "https://social.wikimedia.de/system/cache/accounts/avatars/109/461/452/092/383/527/original/1fcc5cd28206d305.png",
      "avatar_static" =>
        "https://social.wikimedia.de/system/cache/accounts/avatars/109/461/452/092/383/527/original/1fcc5cd28206d305.png",
      "bot" => false,
      "created_at" => "2022-11-07T00:00:00.000Z",
      "discoverable" => true,
      "display_name" => "Testy Test",
      "emojis" => [],
      "fields" => [
        %{
          "name" => "pronouns",
          "value" => "they",
          "verified_at" => nil
        }
      ],
      "followers_count" => 2,
      "following_count" => 3,
      "group" => false,
      "header" =>
        "https://social.wikimedia.de/system/cache/accounts/headers/109/461/452/092/383/527/original/5a756631344dbef4.png",
      "header_static" =>
        "https://social.wikimedia.de/system/cache/accounts/headers/109/461/452/092/383/527/original/5a756631344dbef4.png",
      "id" => "109461452092383527",
      "last_status_at" => "2024-08-04",
      "locked" => false,
      "note" => "<p>Testabout.</p>",
      "statuses_count" => 123,
      "uri" => "https://mastodon.test/users/testy",
      "url" => "https://mastodon.test/@testy",
      "username" => "testy"
    },
    "bookmarked" => false,
    "card" => nil,
    "content" => "<p>Test content.</p>",
    "created_at" => "2024-08-04T21:41:45.000Z",
    "edited_at" => "2024-08-04T21:49:11.000Z",
    "emojis" => [],
    "favourited" => false,
    "favourites_count" => 0,
    "filtered" => [],
    "id" => "112905929331638830",
    "in_reply_to_account_id" => nil,
    "in_reply_to_id" => nil,
    "language" => nil,
    "media_attachments" => [],
    "mentions" => [],
    "muted" => false,
    "poll" => nil,
    "reblog" => nil,
    "reblogged" => false,
    "reblogs_count" => 0,
    "replies_count" => 0,
    "sensitive" => false,
    "spoiler_text" => "",
    "tags" => [],
    "uri" => "https://mastodon.test/users/testy/statuses/12345/activity",
    "url" => nil,
    "visibility" => "public"
  }

  test "normalize URL with spaces" do
    out = MastodonApi.normalize_base_url(" https://a.invalid/ ")
    assert out == "https://a.invalid"
  end

  test "normalize URL with username" do
    out = MastodonApi.normalize_base_url("https://a.invalid/@b")
    assert out == "https://a.invalid"
  end

  test "normalize URL adds scheme" do
    out = MastodonApi.normalize_base_url("a.invalid")
    assert out == "https://a.invalid"
  end

  test "parse status" do
    assert %MastodonNormalizer.Status{
             account: %MastodonNormalizer.Account{
               acct: "testy@mastodon.test",
               display_name: "Testy Test",
               id: "109461452092383527",
               url: "https://mastodon.test/@testy"
             },
             content: "<p>Test content.</p>",
             favourited: false,
             id: "112905929331638830",
             media_attachments: [],
             muted: false,
             reblog: nil,
             sensitive: false,
             spoiler_text: nil,
             uri: "https://mastodon.test/users/testy/statuses/12345/activity",
             url: nil,
             visibility: "public",
             created_at: ~N[2024-08-04 21:41:45],
             edited_at: ~N[2024-08-04 21:49:11]
           } == MastodonApi.parse_status(@raw_post)
  end
end
