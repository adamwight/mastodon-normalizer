defmodule MastodonNormalizerWeb.StatusComponentsTest do
  alias MastodonNormalizerWeb.GuidedTour.Samples
  alias MastodonNormalizerWeb.StatusComponents
  alias MastodonNormalizerWeb.StatusRow
  import Phoenix.LiveViewTest
  use ExUnit.Case

  test "image alt" do
    rendered =
      render_component(&StatusComponents.image/1,
        media: %{description: "foo", preview_url: "https://cdn.test/bar.jpg"}
      )

    assert rendered =~ ~s(alt="foo")
    assert rendered =~ ~s(title="foo")
  end

  test "status row" do
    rendered =
      render_component(StatusRow,
        collapsed: false,
        id: "123",
        instance: "https://example.test/",
        posts: Samples.many_posts()
      )

    assert rendered =~ ~s(href="https://example.test//@dgar@aus.social")
    assert rendered =~ ~s(href="https://example.test//@firstdogonthemoon@aus.social")
    assert rendered =~ "<p>Sustained light injuries.</p>"
    assert rendered =~ "250 years old"
  end
end
