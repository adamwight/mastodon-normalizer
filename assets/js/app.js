// If you want to use Phoenix channels, run `mix help phx.gen.channel`
// to get started and then uncomment the line below.
// import "./user_socket.js"

// You can include dependencies in two ways.
//
// The simplest option is to put them in assets/vendor and
// import them using relative paths:
//
//     import "../vendor/some-package.js"
//
// Alternatively, you can `npm install some-package --prefix assets` and import
// them using a path starting with the package name:
//
//     import "some-package"
//

// Include phoenix_html to handle method=PUT/DELETE in forms and buttons.
import "phoenix_html"
// Establish Phoenix Socket and LiveView configuration.
import { Socket } from "phoenix"
import { LiveSocket } from "phoenix_live_view"
import topbar from "../vendor/topbar"

let Hooks = {}

const expandRow = row => {
  // FIXME: this is a nuisance on the help pages
  // TODO: safer coordination with StatusRow component
  const collapsible = row.children.namedItem("collapsible-row")
  if ( collapsible ) {
    collapsible.classList.remove("h-48", "focus-within:h-auto", "overflow-y-hidden")
    collapsible.classList.add("h-auto")
  }
}

Hooks.StatusRow = {
  mounted() {
    this.el.addEventListener("focusin", e => {
      // TODO: Can this be simplified by moving some to phx-focus?
      const row = e.target.closest('article.status-row')
      if (row) {
        expandRow(row)

        this.pushEvent("focus_row", {row_id: row.id}, (reply, _ref) => {
          // TODO: is this before or after expanding?
          row.scrollIntoView({
            behavior: "smooth",
            block: "nearest"
          })
        })
      }
    })
  }
}

window.addEventListener("phx:focus-row", e => {
  const row = document.getElementById(e.detail.row_id)
  if (row) {
    row.focus()
    // expandRow(row)
  }
})

Hooks.GuidedTourOnFocus = {
  mounted() {
    this.el.addEventListener("focusin", e => {
      document.getElementById("is-done").classList.remove("hidden")
      document.getElementById("loader").classList.add("hidden")
    })
  }
}

/* FIXME: doesn't work yet
window.addEventListener("scroll", e => {
  // FIXME: store the cursor'd row ID so it can be found here.
  const row = document.querySelector(".hover\\:overflow-x-visible:hover")
  console.log(row, window.scrollX)
  if (row !== null && window.scrollX != 0) {
    const adj = window.scrollX
    window.scroll({left: 0})
    row.style.margin.left -= adj
  }
  // TODO: this will be fun!  Make cursor and all other timelines scrob
  // simultaneously, according to user prefs.
  // this.pushEvent("time-scrob", {})
})
*/

let csrfToken = document.querySelector("meta[name='csrf-token']").getAttribute("content")
let liveSocket = new LiveSocket("/live", Socket, { params: { _csrf_token: csrfToken }, hooks: Hooks })

// Show progress bar on live navigation and form submits
topbar.config({ barColors: { 0: "#29d" }, shadowColor: "rgba(0, 0, 0, .3)" })
window.addEventListener("phx:page-loading-start", _info => topbar.show(300))
window.addEventListener("phx:page-loading-stop", _info => topbar.hide())

// connect if there are any LiveViews on the page
liveSocket.connect()

// expose liveSocket on window for web console debug logs and latency simulation:
// >> liveSocket.enableDebug()
// >> liveSocket.enableLatencySim(1000)  // enabled for duration of browser session
// >> liveSocket.disableLatencySim()
window.liveSocket = liveSocket
