# Mastodon Feed Normalizer

Prototype service which flattens a timeline into equal space per author.

## Running

To start your Phoenix server:

  * Run `mix setup` to install and setup dependencies
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Translating

To update the gettext strings:

  mix gettext.extract --merge

Add a new language (here, `es`):

  msginit -i priv/gettext/default.pot --locale=es -o priv/gettext/es/LC_MESSAGES/default.po
