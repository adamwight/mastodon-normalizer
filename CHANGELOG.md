# Changelog

## [Unreleased]

...

## [0.3.9] - 2024-11

### Added

- Dark mode support

## TODO: document releases 0.3.2-0.3.8

## [0.3.1] - 2024-07

### Added

- Short guided tour of the orthogonal view.

## [0.2.x] - 2024-05 through 2024-07

### Added

- Preference for random row order.
- Hover and focus styling.
- Preference for row collapse.
- Preference for row sort.
- Click to focus each row
- Location fragment is set on row click.  Incomplete feature.

### Changed

- Collapsed content fades at edge, to hint expanding.
- Update translations.
- Snap to row on click.  Still buggy.
- Layout improvements suggested by Jan.
- Base async results on new, native Live View async.
- Reverse timeline by default, so time increases to the right.
- Speed up mode changes
- Upgrade to Live View 0.20
- Layout improvements

### Removed

- Added and removed the Oban library.

## [0.1.x] - 2023-03 through 2024-04

### Added

- Heart (favorite) button is live
- Immediate, client-side feedback when opening config
- Horizontal scrolling snaps to card edge.
- Track and publish prometheus-compatible metrics at `/metrics`.
- Authors are linked to the local and global profile, in a new tab.
- Locale switcher
- Rough translations into German and Spanish

### Fixed

- Fall back from display name to username
- Hide some egregious flash
- Long words are line-wrapped.
- Content warning mask is applied to full content on small screens.
- Translation regression fixed.
- Highlighting of navigation buttons fixed.
- Server error when logging in for the first time.
- Crash gracefully and redirect home with flash if login errors out.

### Changed

- Visually encapsulate config block
- Refactor to generalize config
- Settings cog is collapsed by default.
- More semantic document structure.
- Default to alphabetical order until randomness can be tamed.
- Fetch the latest 200 statuses rather than 40.
- Improved small-screen styling, keep posts to page width, tighten margins.
- Temporarily breaks interface translation on the feed page
- Sideways scrolling is isolated by author.
- Internal logging improvements
- Slightly improve button layout
- All feed paths are now under `/feed/:algorithm`.  Old paths are redirected.

## [0.1.1] - 2023-03

First public release, announced on a Fediverse reply thread.

### Changed

- Make layout more responsive, for nav buttons and check-in view.

## [0.1.0] - 2023-03

Not announced.

### Added

- Includes three view modes to compare: linear timeline, check-in, and orthogonal axes.
- User can authorize read actions as self on any Mastodon instance.
- Show last 40 statuses from user's home timeline.
- Reflect favourited.
- Link to user's local and author's global pages for each status.

[Unreleased]: https://gitlab.com/adamwight/mastodon-normalizer/-/compare/v0.3.1...HEAD
[0.3.1]: https://gitlab.com/adamwight/mastodon-normalizer/-/compare/v0.3.0...v0.3.1
[0.2.x]: https://gitlab.com/adamwight/mastodon-normalizer/-/compare/v0.1.12...v0.2.9
[0.1.x]: https://gitlab.com/adamwight/mastodon-normalizer/-/compare/v0.1.1...v0.1.12
[0.1.1]: https://gitlab.com/adamwight/mastodon-normalizer/-/compare/v0.1.0...v0.1.1
[0.1.0]: https://gitlab.com/adamwight/mastodon-normalizer/-/tags/v0.1.0
