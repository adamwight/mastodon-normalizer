import Config

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :mastodon_normalizer, MastodonNormalizer.Repo,
  username: "postgres",
  password: "postgres",
  database: "mastodon_normalizer_test",
  hostname: if(System.get_env("CI"), do: "postgres", else: "localhost"),
  pool: Ecto.Adapters.SQL.Sandbox,
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :mastodon_normalizer, MastodonNormalizerWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "Ls7wS5eAlkubs1RwA36NsmICEGQSIPSRHAAUc8mXPpmVty4wakk8aopDlhI8Qdez",
  server: false

# In test we don't send emails.
config :mastodon_normalizer, MastodonNormalizer.Mailer, adapter: Swoosh.Adapters.Test

# Disable swoosh api client as it is only required for production adapters.
config :swoosh, :api_client, false

# Print only warnings and errors during test
config :logger, level: :warning

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime
