defmodule MastodonNormalizer.Repo.Migrations.AddAppsTable do
  use Ecto.Migration

  def change do
    create table("apps") do
      add :base_url, :string, size: 1024
      add :client_id, :string, size: 1024
      add :client_secret, :string, size: 1024
      add :name, :string, size: 256
      add :remote_id, :string, size: 256

      timestamps()
    end
  end
end
