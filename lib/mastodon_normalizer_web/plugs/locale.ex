defmodule MastodonNormalizerWeb.Plugs.Locale do
  import Plug.Conn

  @behaviour Plug

  def init(_), do: nil

  def call(conn, _) do
    # TODO: browser locale fallback
    locale = get_session(conn, :locale) || "en"
    Gettext.put_locale(locale)

    assign(conn, :locale, locale)
  end
end
