defmodule MastodonNormalizerWeb.Plugs.BootstrapUser do
  import Plug.Conn
  alias Ueberauth.Auth.Credentials
  alias Ueberauth.Strategy.Mastodon

  @behaviour Plug

  def init(_), do: nil

  def call(conn, _) do
    # Get the token set from the callback
    case get_session(conn, :token_data) do
      nil -> conn
      # Make an HTTP request
      %Credentials{token: token} -> verify_token(conn, token)
      # Delete invalid token
      _ -> delete_session(conn, :token_data)
    end
  end

  # Fetch the account from the token
  defp verify_token(conn, token) do
    # FIXME: token_data uid should be an ActivityPub ID, which woule serve as a
    # convenient base URL
    ap_id = get_session(conn, :uid)
    instance = get_session(conn, :instance)

    with {:ok, %{status: 200, body: %{"url" => ^ap_id} = data}} <-
           Mastodon.API.account_verify_credentials(instance, token) do
      conn
      |> assign(:user_data, data)
      # FIXME: How to pass data to the live view?
      |> put_session(:my_name, data["display_name"])
    else
      _ -> delete_session(conn, :token_data)
    end
  end
end
