defmodule MastodonNormalizerWeb.Router do
  use MastodonNormalizerWeb, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {MastodonNormalizerWeb.Layouts, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug MastodonNormalizerWeb.Plugs.BootstrapUser
    plug MastodonNormalizerWeb.Plugs.Locale
  end

  pipeline :auth do
    plug :ensure_user_authenticated
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", MastodonNormalizerWeb do
    pipe_through :browser

    get "/", PageController, :home
    live "/help", GuidedTour
    get "/logout", AuthController, :logout
  end

  scope "/", MastodonNormalizerWeb do
    pipe_through [:browser, :auth]

    # Grandfathered stale links
    get "/friendplane", LegacyFeedRedirector, :index
    get "/checkin", LegacyFeedRedirector, :index
    get "/timeline", LegacyFeedRedirector, :index
  end

  scope "/feed", MastodonNormalizerWeb do
    pipe_through [:browser, :auth]

    # Grandfathered stale links
    get "/friendplane", LegacyFeedRedirector, :index
    get "/checkin", LegacyFeedRedirector, :index
    get "/timeline", LegacyFeedRedirector, :index

    live "/", Feed
    live "/:row_id", Feed, :row
  end

  scope "/locale", MastodonNormalizerWeb do
    pipe_through [:browser]

    get "/:locale", LocaleController, :set
  end

  # Other scopes may use custom stacks.
  # scope "/api", MastodonNormalizerWeb do
  #   pipe_through :api
  # end

  scope "/auth", MastodonNormalizerWeb do
    pipe_through [:browser, Ueberauth]

    get "/mastodon", AuthController, :request
    get "/mastodon/callback", AuthController, :callback
  end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:mastodon_normalizer, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through :browser

      live_dashboard "/dashboard", metrics: MastodonNormalizerWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
