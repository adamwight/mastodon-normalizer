defmodule MastodonNormalizerWeb.Feed do
  alias Phoenix.LiveView.AsyncResult
  use MastodonNormalizerWeb, :live_view
  alias MastodonNormalizer.MastodonApi
  alias MastodonNormalizerWeb.StatusRow
  alias MastodonNormalizerWeb.Configuration
  require Logger

  @flash_duration 3000

  @impl true
  def mount(_params,
        %{"instance" => instance, "my_name" => my_name, "token_data" => %{token: token}} =
          session,
        socket
      ) do
    # FIXME: Should have been set by the plug--is this a different process?
    locale = Map.get(session, "locale", "en")
    Gettext.put_locale(locale)

    Process.send_after(self(), :clear_flash, @flash_duration)

    config =
      %{
        is_config_open: false,
        locale: locale,
        row_sort: "active",
        post_sort: "newest",
        collapse_y: "rigid",
        limit: 5
      }
      |> Map.merge(Map.get(session, "config", %{}))

    {
      :ok,
      socket
      |> assign(:config, config)
      |> assign(:my_name, my_name)
      |> assign(:instance, instance)
      # FIXME: It should be possible to limit token exposure better than this?
      |> assign(:token, token)
      |> assign(:focused_row, nil)
      |> assign(:latest, AsyncResult.ok([]))
      |> assign(:load_status, %{remaining: config.limit})
      # FIXME: starts twice?
      |> start_async(:get_latest, fn -> fetch_home_timeline(instance, token, config.limit) end)
    }
  end

  @impl true
  def handle_params(%{"row_id" => row_id}, _uri, socket) do
    {:noreply,
     socket
     |> assign(:focused_row, row_id)}
  end

  # TODO: can it be dropped?
  def handle_params(%{}, _uri, socket) do
    {:noreply, socket}
  end

  defp fetch_home_timeline(instance, token, rounds, max_status_id \\ "")

  defp fetch_home_timeline(instance, token, rounds, max_status_id) do
    timeline =
      :telemetry.span([:api, :fetch_timeline], %{}, fn ->
        Logger.debug("get_home_timeline from #{max_status_id}")

        {
          MastodonApi.get_home_timeline(instance, token, max_status_id),
          %{is_initial: max_status_id == ""}
        }
      end)

    {timeline, rounds - 1}
  end

  defp uri_encode_acct(str) do
    # As strict as VerifiedRoutes but allow the unescaped at-sign "@"
    URI.encode(str, &(URI.char_unreserved?(&1) or &1 === ?@))
  end

  @impl true
  def handle_event("focus_row", %{"row_id" => row_id}, socket) do
    {:noreply,
     socket
     |> push_patch(to: "/feed/#{uri_encode_acct(row_id)}")
     |> assign(:focused_row, row_id)}
  end

  def handle_event("add_favorite", %{"post" => post_id}, socket) do
    MastodonApi.add_favorite(socket.assigns.instance, socket.assigns.token, post_id)
    # TODO: client feedback that the action succeeded
    {:noreply, socket}
  end

  def handle_event("toggle_config", %{"show" => ""}, socket),
    do:
      {:noreply,
       socket
       |> assign(:config, %{socket.assigns.config | is_config_open: true})}

  def handle_event("toggle_config", %{}, socket),
    do:
      {:noreply,
       socket
       |> assign(:config, %{socket.assigns.config | is_config_open: false})}

  def handle_event("set_limit", %{"limit" => limit}, socket) do
    {limit, ""} = Integer.parse(limit)
    config = Map.replace!(socket.assigns.config, :limit, limit)
    instance = socket.assigns.instance
    token = socket.assigns.token

    # FIXME: simply truncate if the limit is decreased
    {:noreply,
     socket
     |> assign(:latest, AsyncResult.ok([]))
     |> assign(:config, config)
     |> start_async(:get_latest, fn ->
       fetch_home_timeline(instance, token, config.limit)
     end)}
  end

  def handle_event("set_row_collapse", %{"collapse" => collapse}, socket),
    do:
      {:noreply,
       socket
       |> assign(:config, %{socket.assigns.config | collapse_y: collapse})}

  def handle_event("set_row_sort", %{"sort" => sort}, socket),
    do:
      {:noreply,
       socket
       |> assign(:config, %{socket.assigns.config | row_sort: sort})}

  def handle_event("set_post_sort", %{"sort" => sort}, socket),
    do:
      {:noreply,
       socket
       |> assign(:config, %{socket.assigns.config | post_sort: sort})}

  # TODO
  # def handle_async(:get_latest, {:error, _}, socket) do

  @impl true
  def handle_async(:get_latest, {:ok, {timeline, 0}}, socket) do
    {:noreply,
     socket
     |> assign(
       :latest,
       AsyncResult.ok(
         (timeline ++ socket.assigns.latest.result)
         |> Enum.sort_by(& &1.created_at)
         |> Enum.reverse()
       )
     )
     |> assign(:load_status, nil)
     |> maybe_focus(socket.assigns.focused_row)}
  end

  def handle_async(:get_latest, {:ok, {timeline, remaining}}, socket) when remaining > 0 do
    new_max =
      case List.last(timeline) do
        nil -> :stop
        last_status -> last_status.id
      end

    instance = socket.assigns.instance
    token = socket.assigns.token

    {:noreply,
     socket
     |> assign(:latest, AsyncResult.ok(timeline ++ socket.assigns.latest.result))
     |> maybe_focus(socket.assigns.focused_row)
     |> assign(:load_status, %{remaining: remaining})
     |> start_async(:get_latest, fn ->
       fetch_home_timeline(instance, token, remaining, new_max)
     end)}
  end

  defp maybe_focus(socket, nil), do: socket

  # defp maybe_focus(socket, row_id, fresh_posts) do
  defp maybe_focus(socket, row_id) do
    push_event(socket, "focus-row", %{row_id: row_id})
  end

  @impl true
  def handle_info(:clear_flash, socket) do
    {:noreply,
     socket
     |> clear_ephemeral_flash()}
  end

  def handle_info({:DOWN, _, :process, _, _}, socket) do
    {:noreply, socket}
  end

  defp clear_ephemeral_flash(%{assigns: %{flash: %{"error" => _}}} = socket),
    do: socket

  defp clear_ephemeral_flash(socket),
    do: clear_flash(socket)

  @impl true
  def render(assigns) do
    ~H"""
    <article>
      <.live_component module={Configuration} id="cfg" config={@config} />
      <.loading_status status={@load_status} />
      <.view_header config={@config} my_name={@my_name} />
      <.async_result :let={latest} assign={@latest}>
        <:loading>{gettext("Loading messages...")}</:loading>
        <:failed :let={_failure}>{gettext("Couldn't fetch messages!")}</:failed>
        <%= if latest do %>
          <.list_posts
            config={@config}
            focused_row={@focused_row}
            latest={
              case @config.post_sort do
                "oldest" -> Enum.reverse(latest)
                "newest" -> latest
              end
            }
            instance={@instance}
          />
        <% end %>
      </.async_result>
    </article>
    """
  end

  defp loading_status(%{status: nil} = assigns), do: ~H""

  defp loading_status(assigns) do
    ~H"""
    <div class="float-right mr-10 mt-2">
      {gettext("Fetching %{remaining} posts…",
        remaining: @status.remaining * MastodonApi.get_batch_size()
      )}
    </div>
    """
  end

  defp view_header(assigns) do
    ~H"""
    <.feed_header
      help={
        gettext(
          "Latest %{limit} posts, organized by author. Click to focus a row. Scroll sideways to read more.",
          limit: @config.limit * 40
        )
      }
      title={gettext("Orthogonal view for %{name}", name: @my_name)}
    />
    """
  end

  def list_posts(assigns) do
    assigns =
      assigns
      |> assign(
        :by_author,
        build_normalized_timeline(assigns.latest, assigns.config.row_sort)
      )

    ~H"""
    <div class="flex flex-col">
      <%= for {acct, posts} <- @by_author do %>
        <.live_component
          module={StatusRow}
          id={acct}
          instance={@instance}
          collapsed={@config.collapse_y == "rigid" and @focused_row != acct}
          posts={posts}
        />
      <% end %>
    </div>
    """
  end

  defp build_normalized_timeline(%{ok?: false}, _), do: []

  defp build_normalized_timeline(latest, order) do
    # TODO: somehow fetch from all friends, ideally not by scrolling purely statuses.
    # FIXME: specialize out the "group by account" concept. Maybe group map is a
    # better internal format and should be used when available--only complicated
    # by timeline view. maybe timeline rendering is a separate code path

    latest
    |> Enum.group_by(& &1.account.acct)
    |> sort_rows(order)
  end

  defp sort_rows(rows, "random") do
    Enum.shuffle(rows)
  end

  defp sort_rows(rows, order) do
    rows2 =
      Enum.sort_by(rows, fn {_acct, statuses} ->
        cond do
          order in ["alpha", "alpha-reverse"] ->
            account = hd(statuses).account
            String.downcase(account.display_name || account.acct)

          order in ["active", "active-reverse"] ->
            statuses
            |> Enum.map(& &1.created_at)
            |> Enum.max()
        end
      end)

    cond do
      order in ["alpha-reverse", "active"] -> Enum.reverse(rows2)
      true -> rows2
    end
  end
end
