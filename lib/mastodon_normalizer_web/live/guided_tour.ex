defmodule MastodonNormalizerWeb.GuidedTour do
  use MastodonNormalizerWeb, :live_view
  alias MastodonNormalizerWeb.StatusRow
  require Logger

  alias MastodonNormalizerWeb.GuidedTour.Samples

  defp invalid_instance(), do: "https://example.invalid/"

  @impl true
  def mount(
        %{},
        session,
        socket
      ) do
    locale = Map.get(session, "locale", "en")
    Gettext.put_locale(locale)

    {
      :ok,
      socket
      |> assign(:my_name, gettext("example-username"))
      |> assign(:focused_row, nil)
      |> assign(:state, :introduction),
      layout: false
    }
  end

  @impl true
  def handle_event("focus_row", %{"row_id" => row_id}, socket) do
    {:noreply, socket |> assign(:focused_row, row_id)}
  end

  def handle_event("add_favorite", %{"post" => _post_id}, socket) do
    # TODO: implement demo
    {:noreply, socket}
  end

  def handle_event("start", %{}, socket),
    do: {:noreply, socket |> assign(:state, :one_post)}

  def handle_event("next", %{}, %{assigns: %{state: :one_post}} = socket),
    do: {:noreply, socket |> assign(:state, :long_post)}

  def handle_event("next", %{}, %{assigns: %{state: :long_post}} = socket),
    do: {:noreply, socket |> assign(:state, :two_posts)}

  def handle_event("next", %{}, %{assigns: %{state: :two_posts}} = socket),
    do: {:noreply, socket |> assign(:state, :side_scroll)}

  def handle_event("next", %{}, %{assigns: %{state: :side_scroll}} = socket),
    do: {:noreply, socket |> assign(:state, :done)}

  def handle_event("next", %{}, %{assigns: %{state: :done}} = socket),
    do: {:noreply, socket |> redirect(to: ~p"/")}

  @impl true
  def handle_info({:DOWN, _, :process, _, _}, socket) do
    {:noreply, socket}
  end

  # FIXME: focused_row is gross
  @impl true
  def render(assigns),
    do: ~H"""
    <main class="px-4 py-2 md:py-4 sm:px-6 lg:px-8 bg-emerald-200 dark:bg-emerald-800 min-h-screen h-full">
      <article class="bg-zinc-100 dark:bg-zinc-900 p-2 md:p-4">
        <div class="block m-4">
          <.step_content state={assigns.state} focused_row={@focused_row} />
        </div>
      </article>
    </main>
    """

  defp step_content(%{state: :introduction} = assigns),
    do: ~H"""
    <p class="m-2 italic">
      {gettext("This guided tour will demonstrate how to use the \"orthogonal\" view.")}
    </p>
    <p class="m-2 italic">
      {gettext("Please click begin to start the tour.")}
    </p>
    <div class="relative">
      <div class="absolute left-0">
        <div class="loader"></div>
      </div>
      <div class="relative z-1 mx-4 py-4">
        <.begin_button />
      </div>
    </div>
    """

  defp step_content(%{state: :one_post} = assigns),
    do: ~H"""
    <p class="m-2 italic">
      {gettext("Here is one post.")}
    </p>
    <p class="m-2 italic">
      <.icon id="is-done" name="hero-check" class="hidden w-5 h-5" />
      {gettext("Click on the row to focus and expand it vertically.")}
    </p>
    <div class="absolute left-64">
      <div id="loader" class="loader"></div>
    </div>
    <div phx-hook="GuidedTourOnFocus" id="guided-tour-posts">
      <.list_posts latest={Samples.one_post()} focused_row={@focused_row} />
    </div>
    <.next_button />
    """

  defp step_content(%{state: :long_post} = assigns),
    do: ~H"""
    <p class="m-2 italic">
      {gettext("Collapsing rows makes a big difference for tall posts.")}
    </p>
    <p class="m-2 italic">
      {gettext("(You can only collapse an expanded row again by clicking on a different row.)")}
    </p>
    <div phx-hook="GuidedTourOnFocus" id="guided-tour-posts">
      <.list_posts latest={Samples.long_post()} focused_row={@focused_row} />
    </div>
    <.next_button />
    """

  defp step_content(%{state: :two_posts} = assigns),
    do: ~H"""
    <p class="m-2 italic">
      {gettext(~s(
        Same thing when you have more posts, but notice that only one
        author can be focused at a time.
      ))}
    </p>
    <.list_posts latest={Samples.two_posts_different_authors()} focused_row={@focused_row} />
    <.next_button />
    """

  defp step_content(%{state: :side_scroll} = assigns),
    do: ~H"""
    <p class="m-2 italic">
      {gettext(~s(
        But multiple posts by the same author appear in the same row,
        extending off to the sides. This is unique to the orthogonal view.
      ))}
    </p>
    <p class="m-2 italic">
      {gettext(~s(
        Try scrolling sideways to read more. On a touch device, this should
        be a normal dragging motion. Try holding down the shift key and
        using the scroll wheel. As a worst-case fallback, use the row's
        scroll bar.
      ))}
    </p>
    <.list_posts latest={Samples.many_posts()} focused_row={@focused_row} />
    <.next_button />
    """

  defp step_content(%{state: :done} = assigns),
    do: ~H"""
    <p class="m-2 mb-6">
      {gettext(~s(
            That's all for now, thanks!  To get back to the application, click next.
          ))}
    </p>
    <.next_button />
    """

  def begin_button(assigns),
    do: ~H"""
    <div
      class="cursor-pointer bg-black rounded-xl inline-block px-4 md:px-6 py-2 text-lg font-semibold text-white"
      phx-click={JS.hide() |> JS.push("start")}
    >
      {gettext("Begin tour")}
    </div>
    """

  attr :rest, :global

  def prev_button(assigns),
    do: ~H"""
    <div
      class={[
        @rest[:class],
        "my-4 md:my-8 cursor-pointer bg-blue-500 rounded-xl inline-block px-4 md:px-6 py-2 text-lg font-semibold text-white"
      ]}
      phx-click={JS.push("prev")}
    >
      {gettext("Previous")}
    </div>
    """

  attr :rest, :global

  def next_button(assigns),
    do: ~H"""
    <div
      class={[
        @rest[:class],
        "my-4 md:my-8 cursor-pointer bg-blue-500 rounded-xl inline-block px-4 md:px-6 py-2 text-lg font-semibold text-white"
      ]}
      phx-click={JS.push("next")}
    >
      {gettext("Next")}
    </div>
    """

  def list_posts(assigns) do
    assigns =
      assigns
      |> assign(
        :by_author,
        build_normalized_timeline(assigns.latest)
      )

    ~H"""
    <div class="flex flex-col bg-white dark:bg-zinc-900">
      <%= for posts <- @by_author do %>
        <.live_component
          module={StatusRow}
          id={"friendplane-#{hd(posts).account.id}"}
          instance={invalid_instance()}
          collapsed={@focused_row != "friendplane-#{hd(posts).account.id}"}
          posts={posts}
        />
      <% end %>
    </div>
    """
  end

  defp build_normalized_timeline(latest) do
    # TODO: somehow fetch from all friends, ideally not by scrolling purely statuses.

    latest
    |> Enum.group_by(& &1.account.url)
    |> Map.values()
    |> Enum.sort_by(fn statuses ->
      account = List.first(statuses).account
      String.downcase(account.display_name)
    end)
  end
end

# FIXME: expand state to a machine
# defmodule MastodonNormalizerWeb.GuidedTour.StateMachine do
#   @behaviour :gen_statem
#
#     @impl true
#   def init(args),
#     do: {:ok, :introduction, args}
#
#   @impl true
#   def callback_mode() do
#     [:state_functions, :state_enter]
#   end
#
#   def introduction(:enter, :introduction, _) do
#     {:keep_state_and_data}
#   end
# end
