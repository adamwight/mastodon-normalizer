defmodule MastodonNormalizerWeb.GuidedTour.Samples do
  use MastodonNormalizerWeb, :verified_routes

  def one_post() do
    [
      %MastodonNormalizer.Status{
        account: %MastodonNormalizer.Account{
          acct: "a",
          display_name: "a wight",
          id: "109525794460130028",
          url: "https://social.wikimedia.de/@a"
        },
        content:
          "<p>Radical solution to closing the gender pay gap: pay women, divers, non-binary people more, promote, hire.</p><p>Often the inequalities are hidden behind job titles—so pay all jobs the same.</p>",
        favourited: false,
        id: "112625135441080446",
        media_attachments: [],
        reblog: nil,
        spoiler_text: nil,
        url: "https://social.wikimedia.de/@a/112625135441080446"
      }
    ]
  end

  def long_post() do
    [
      %MastodonNormalizer.Status{
        account: %MastodonNormalizer.Account{
          acct: "a",
          display_name: "adamw",
          id: "999109525794460130028",
          url: "https://social.wikimedia.invalid/@a"
        },
        content:
          "<p>Making these bookshelves was a blast.  Here are two of the jigs (a third jig shaped like a ladder is too mundane to be pictured).</p>",
        favourited: true,
        id: "999112625135441080446",
        media_attachments: [
          %MastodonNormalizer.Media{
            description:
              "A pair of handmade metal shelf brackets which weave back and forth with two supports jutting out to support each shelf",
            id: "999112798802935832660",
            preview_url: ~p"/assets/samples/shelf-brackets.jpg",
            type: "image",
            url: ~p"/assets/samples/shelf-brackets.jpg"
          },
          %MastodonNormalizer.Media{
            description: "Spiraling metal jig for forging a scroll",
            id: "999112798802935832661",
            preview_url: ~p"/assets/samples/scroll-jig.jpg",
            type: "image",
            url: ~p"/assets/samples/scroll-jig.jpg"
          },
          %MastodonNormalizer.Media{
            description:
              "Metal jig for welding a bookshelf support foot at 45 degrees to its leg",
            id: "999112798802935832662",
            preview_url: ~p"/assets/samples/foot-jig.jpg",
            type: "image",
            url: ~p"/assets/samples/foot-jig.jpg"
          }
        ],
        reblog: nil,
        spoiler_text: nil,
        url: "https://social.wikimedia.de/@a/999112625135441080446"
      }
    ]
  end

  def two_posts_different_authors() do
    one_post() ++ long_post()
  end

  def many_posts() do
    [
      %MastodonNormalizer.Status{
        account: %MastodonNormalizer.Account{
          acct: "dgar@aus.social",
          display_name: "Dgar",
          id: "109465953424199820",
          url: "https://aus.social/@dgar"
        },
        content: "<p>Walked into a lamppost. </p><p>Sustained light injuries.</p>",
        favourited: false,
        id: "112801608939593422",
        media_attachments: [],
        reblog: nil,
        spoiler_text: nil,
        url: "https://aus.social/@dgar/112801608590841123"
      },
      %MastodonNormalizer.Status{
        account: %MastodonNormalizer.Account{
          acct: "dgar@aus.social",
          display_name: "Dgar",
          id: "109465953424199820",
          url: "https://aus.social/@dgar"
        },
        content: "<p>Exaggerations went up by a million percent last year.</p>",
        favourited: false,
        id: "112801594875375658",
        media_attachments: [],
        reblog: nil,
        spoiler_text: nil,
        url: "https://aus.social/@dgar/112801594500603959"
      },
      %MastodonNormalizer.Status{
        account: %MastodonNormalizer.Account{
          acct: "dgar@aus.social",
          display_name: "Dgar",
          id: "109465953424199820",
          url: "https://aus.social/@dgar"
        },
        content: "",
        favourited: false,
        id: "112801404283797086",
        media_attachments: [],
        reblog: %MastodonNormalizer.Status{
          account: %MastodonNormalizer.Account{
            acct: "KydiaMusic@mastodon.social",
            display_name: "Kydia Music",
            id: "110237128217522367",
            url: "https://mastodon.social/@KydiaMusic"
          },
          content:
            "<p>How ‘bout instead of machines that steal from artists to “democratize creativity” we have machines that steal from billionaires to democratize money</p>",
          favourited: false,
          id: "112798929948594150",
          media_attachments: [],
          reblog: nil,
          spoiler_text: nil,
          url: "https://mastodon.social/@KydiaMusic/112798929972821014"
        },
        spoiler_text: nil,
        url: nil
      },
      %MastodonNormalizer.Status{
        account: %MastodonNormalizer.Account{
          acct: "dgar@aus.social",
          display_name: "Dgar",
          id: "109465953424199820",
          url: "https://aus.social/@dgar"
        },
        content: "",
        favourited: false,
        id: "112800975415943921",
        media_attachments: [],
        reblog: %MastodonNormalizer.Status{
          account: %MastodonNormalizer.Account{
            acct: "msdropbear42@anonsys.net",
            display_name: "Droppie [anonsys] 🐨♀🌈🐧​🦘",
            id: "112479049156996168",
            url: "https://anonsys.net/profile/msdropbear42"
          },
          content:
            "<p>I had some spare thyme, so thought it would be sage to top up my rosemary jar, but now Sybil is screeching something at me. Maybe i'm being too parselymonious...</p><p><a href=\"https://anonsys.net/search?tag=Simon%26Garfunkel\">#Simon&amp;Garfunkel</
  a> <a href=\"https://anonsys.net/search?tag=FloweryTwats\">#FloweryTwats</a></p>",
          favourited: false,
          id: "112799756068537486",
          media_attachments: [],
          reblog: nil,
          spoiler_text: nil,
          url: "https://anonsys.net/display/bf69967c-1866-973d-644f-9ee801509367"
        },
        spoiler_text: nil,
        url: nil
      },
      %MastodonNormalizer.Status{
        account: %MastodonNormalizer.Account{
          acct: "dgar@aus.social",
          display_name: "Dgar",
          id: "109465953424199820",
          url: "https://aus.social/@dgar"
        },
        content: "",
        favourited: false,
        id: "112800946638767126",
        media_attachments: [],
        reblog: %MastodonNormalizer.Status{
          account: %MastodonNormalizer.Account{
            acct: "skyfire747@aus.social",
            display_name: "Anthony 🦘🐨🪃🕯🧙‍♂️🔆🇦🇺",
            id: "111556816751868666",
            url: "https://aus.social/@skyfire747"
          },
          content:
            "<p>New <a href=\"https://aus.social/tags/introduction\">#introduction</a> post. I am just someone that loves <a href=\"https://aus.social/tags/trains\">#trains</a>, <a href=\"https://aus.social/tags/ships\">#ships</a>, <a href=\"https://aus.social/tags/l
  ego\">#lego</a>, <a href=\"https://aus.social/tags/space\">#space</a> and <a href=\"https://aus.social/tags/travel\">#travel</a>.</p>",
          favourited: false,
          id: "111556329041277387",
          media_attachments: [],
          reblog: nil,
          spoiler_text: nil,
          url: "https://aus.social/@skyfire747/111556329024039335"
        },
        spoiler_text: nil,
        url: nil
      },
      %MastodonNormalizer.Status{
        account: %MastodonNormalizer.Account{
          acct: "dgar@aus.social",
          display_name: "Dgar",
          id: "109465953424199820",
          url: "https://aus.social/@dgar"
        },
        content:
          "<p>\"Orion's Belt is a big waist of space.\" </p><p>Terrible joke. </p><p>Only three stars.</p>",
        favourited: false,
        id: "112800811837045453",
        media_attachments: [],
        reblog: nil,
        spoiler_text: nil,
        url: "https://aus.social/@dgar/112800811495085778"
      },
      %MastodonNormalizer.Status{
        account: %MastodonNormalizer.Account{
          acct: "dgar@aus.social",
          display_name: "Dgar",
          id: "109465953424199820",
          url: "https://aus.social/@dgar"
        },
        content: "",
        favourited: false,
        id: "112800805637443362",
        media_attachments: [],
        reblog: %MastodonNormalizer.Status{
          account: %MastodonNormalizer.Account{
            acct: "firstdogonthemoon@aus.social",
            display_name: "Firstdog Onthemoon",
            id: "109528015525362519",
            url: "https://aus.social/@firstdogonthemoon"
          },
          content:
            "<p>Fish! Everyone loves fish! No not like that, to look at and think about them being in the sea. That seafood basket could be 250 years old and not in a good way. A cartoon. <a href=\"https://www.theguardian.com/commentisfree/picture/2024/jul/17/orange-
  roughy-are-at-risk-from-overfishing-but-there-is-money-to-be-made\">https://www.theguardian.com/commentisfree/picture/2024/jul/17/orange-roughy-are-at-risk-from-overfishing-but-there-is-money-to-be-made</a></p>",
          favourited: false,
          id: "112800356214075951",
          media_attachments: [],
          reblog: nil,
          spoiler_text: nil,
          url: "https://aus.social/@firstdogonthemoon/112800356186582403"
        },
        spoiler_text: nil,
        url: nil
      }
    ]
  end
end
