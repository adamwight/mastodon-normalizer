defmodule MastodonNormalizerWeb.PageController do
  use MastodonNormalizerWeb, :controller

  def home(%{assigns: %{user_data: _}} = conn, _params) do
    # FIXME: well actually, on mobile checkin is better for now
    algo = get_session(conn, :view_mode) || :friendplane
    redirect(conn, to: "/feed/#{algo}")
  end

  def home(conn, _params) do
    conn
    # TODO: all params to be handled consistently
    |> assign(
      :instance,
      get_session(conn, :instance) || "https://social.wikimedia.de"
    )
    |> render(:home, layout: false)
  end
end
