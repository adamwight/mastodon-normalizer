defmodule MastodonNormalizerWeb.AuthController do
  use MastodonNormalizerWeb, :controller
  use Gettext, backend: MastodonNormalizerWeb.Gettext

  alias Ueberauth.Auth
  alias Ueberauth.Auth.Credentials
  alias Ueberauth.Failure
  alias Ueberauth.Failure.Error

  alias MastodonNormalizer.MastodonApi

  @provider_name "mastodon"

  def ensure_user_authenticated(%{assigns: %{user_data: _}} = conn, _opts) do
    conn
  end

  def ensure_user_authenticated(conn, _opts) do
    redirect(conn, to: "/")
  end

  # /auth/:provider
  def request(conn, %{"instance" => instance}) do
    instance = MastodonApi.normalize_base_url(instance)

    # FIXME: Better error handling, without "try"
    try do
      conn
      |> put_session(:instance, instance)
      |> Ueberauth.run_request(@provider_name, provider_config(instance))
    rescue
      _ ->
        conn
        |> put_flash(:error, "Something is wrong with your login instance \"#{instance}\"?")
        |> redirect(to: ~p"/")
    end
  end

  def do_callback(
        # An `:ueberauth_auth` key is provided upon success.
        # It contains a `%Ueberauth.Auth{}` struct.
        # https://hexdocs.pm/ueberauth/Ueberauth.Auth.html#t:t/0
        %{assigns: %{ueberauth_auth: %Auth{uid: uid, credentials: %Credentials{} = credentials}}} =
          conn,
        _params
      ) do
    conn
    # Store the credentials in a cookie, or anywhere else
    |> put_session(:token_data, credentials)
    |> put_session(:uid, uid)
    |> put_flash(:info, gettext("Logged in as %{uid}", uid: uid))
    |> redirect(to: "/")
  end

  def do_callback(
        # Upon failure, you'll get `:ueberauth_failure`.
        # It contains a `%Ueberauth.Failure{}` struct.
        # https://hexdocs.pm/ueberauth/Ueberauth.Failure.html#t:t/0
        %{assigns: %{ueberauth_failure: %Failure{errors: [%Error{message: message} | _]}}} = conn,
        _params
      ) do
    conn
    |> put_flash(:error, message)
    |> redirect(to: "/")
  end

  # /auth/:provider/callback
  # After the user authorizes the OAuth form, they'll be redirected back here.
  # TODO: If neither exist, just redirect home
  def callback(conn, %{"error_description" => message}) do
    conn
    |> put_flash(:error, message)
    |> redirect(to: "/")
  end

  def callback(conn, params) do
    instance = get_session(conn, :instance)

    conn
    |> Ueberauth.run_callback(@provider_name, provider_config(instance))
    |> do_callback(params)
  end

  defp provider_config(instance) do
    MastodonApi.provider_config(instance, callback_uri())
  end

  def callback_uri(), do: url(~p"/auth/mastodon/callback")

  def logout(%{assigns: %{user_data: _}} = conn, _) do
    # FIXME: error fall-throughs and handling
    instance = get_session(conn, :instance)
    credentials = get_session(conn, :token_data)
    MastodonApi.revoke_token(instance, callback_uri(), credentials.token)

    conn
    |> delete_session(:token_data)
    |> delete_session(:uid)
    |> put_flash(:info, gettext("Logged out"))
    |> redirect(to: "/")
  end

  def logout(conn, _), do: redirect(conn, to: "/")
end
