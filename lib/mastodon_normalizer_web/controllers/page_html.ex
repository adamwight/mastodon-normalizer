defmodule MastodonNormalizerWeb.PageHTML do
  use MastodonNormalizerWeb, :html

  def home(assigns) do
    ~H"""
    <.flash_group flash={@flash} />
    <div class="px-4 py-4 md:px-8 md:py-10">
      <div class="mx-auto max-w-xl lg:mx-0">
        <.login last_instance={@instance} />
        <.intro />
      </div>
    </div>
    """
  end
end
