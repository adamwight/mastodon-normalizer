defmodule MastodonNormalizerWeb.LocaleController do
  use MastodonNormalizerWeb, :controller

  def set(conn, %{"locale" => locale}) do
    conn
    |> put_session(:locale, locale)
    |> redirect(to: ~p"/")
  end
end
