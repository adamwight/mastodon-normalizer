defmodule MastodonNormalizerWeb.LegacyFeedRedirector do
  use MastodonNormalizerWeb, :controller

  def index(conn, _params), do: redirect(conn, to: ~p"/feed")
end
