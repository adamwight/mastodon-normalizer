defmodule MastodonNormalizerWeb.StatusComponents do
  use Phoenix.Component

  import MastodonNormalizerWeb.CoreComponents
  use Gettext, backend: MastodonNormalizerWeb.Gettext
  alias Phoenix.LiveView.JS
  alias MastodonNormalizer.Emoji

  attr :media, :map, required: true
  attr :collapsed, :boolean, default: false

  def media(%{collapsed: false} = assigns) do
    ~H"""
    <.link href={@media.url} target="_blank">
      <.image media={@media} />
    </.link>
    """
  end

  def media(assigns) do
    ~H"""
    <.image media={@media} />
    """
  end

  def image(assigns) do
    ~H"""
    <img
      src={@media.preview_url}
      alt={@media.description}
      title={@media.description}
      height="110"
      width="239"
    />
    """
  end

  def get_name(account) do
    [
      account.display_name,
      account.acct
    ]
    |> Enum.find(nil, & &1)
    |> Emoji.replace_shortcodes()
  end

  attr :account, :map, required: true
  attr :local, :string, required: true

  def account(assigns) do
    ~H"""
    <.link href={@local} target="_blank">
      <span class="font-bold">{get_name(@account)}</span>
    </.link>
    <.link href={@account.url} target="_blank">
      <.icon name="hero-home" class="w-4 h-4" />
    </.link>
    """
  end

  attr :spoiler, :string, required: true
  attr :id, :string, required: true
  slot :inner_block, required: true

  @spec spoiler(map) :: Phoenix.LiveView.Rendered.t()
  def spoiler(assigns) do
    ~H"""
    {render_slot(@inner_block)}

    <%= if @spoiler do %>
      <div class="absolute top-0 left-0 w-full h-full p-2 md:p-8" id={"spoiler-#{@id}"}>
        <div class="bg-red-100 dark:bg-red-800 w-full h-full">
          <div class="w-full h-full">
            <div class="m-auto">
              <p class="m-auto w-fit">
                <span class="italic">{@spoiler |> Emoji.replace_shortcodes()}</span>
              </p>
              <p class="m-auto w-fit">
                <button
                  phx-click={JS.hide(transition: "fade-out", to: "#spoiler-#{@id}")}
                  class="bg-slate-400 dark:bg-slate-600 p-2 rounded-md"
                >
                  {gettext("Show more")}
                </button>
              </p>
            </div>
          </div>
        </div>
      </div>
    <% end %>
    """
  end

  attr :instance, :string, required: true
  attr :post, :map, required: true
  attr :is_reblog, :map, default: nil
  attr :collapsed, :boolean, default: false

  def status_card(%{post: %{account: _, reblog: reblog}} = assigns)
      when not is_nil(reblog) do
    # TODO: Can or should this be used more than one level deep?
    # TODO: move to controller?
    assigns
    |> Map.merge(%{
      post: reblog,
      is_reblog: true
    })
    |> status_card()
  end

  def status_card(assigns) do
    # FIXME: only allow <p>, <span>, <br>, and <a> tags
    # TODO: gallery, inline player
    ~H"""
    <article class="status min-w-sm max-w-full md:max-w-md flex-none my-4 md:mx-4 snap-start">
      <!--
      <div class="p-2 text-xs">
      <%= @post.created_at %>
      </div>
      -->
      <div :if={@is_reblog}>
        <.icon name="hero-arrow-path-rounded-square" class="w-4 h-4" />
        <.account account={@post.account} local={"#{@instance}/@#{@post.account.acct}"} />
      </div>
      <div class="max-h-min bg-slate-100 dark:bg-zinc-800 rounded-xl p-2 md:p-8 relative">
        <.spoiler spoiler={@post.spoiler_text} id={@post.id}>
          <div class="content break-words">
            {Phoenix.HTML.raw(@post.content |> Emoji.replace_shortcodes())}
            <%= for media <- @post.media_attachments do %>
              <.media media={media} collapsed={@collapsed} />
            <% end %>
          </div>
        </.spoiler>
      </div>
      <div :if={not @collapsed} class="space-x-8 right-0 p-2 md:px-8">
        <.link
          href={"#{@instance}/@#{@post.account.acct}/#{@post.id}"}
          target="_blank"
          aria-label={gettext("local link")}
        >
          <.icon name="hero-link" class="w-6 h-6" />
        </.link>
        <.link href={@post.url} target="_blank" aria-label={gettext("global link")}>
          <.icon name="hero-globe-asia-australia" class="w-6 h-6" />
        </.link>
        <.icon
          :if={not @post.favourited}
          id={"fav-#{@post.id}"}
          name="hero-heart"
          class="w-6 h-6"
          aria-label={gettext("mark favorite")}
          phx-click={
            JS.push("add_favorite")
            |> JS.add_class("hero-heart-solid bg-blue-500")
          }
          phx-value-post={@post.id}
        />
        <.icon :if={@post.favourited} name="hero-heart-solid" class="w-6 h-6" />
      </div>
    </article>
    """
  end
end

defmodule MastodonNormalizerWeb.StatusRow do
  use Phoenix.LiveComponent
  use Gettext, backend: MastodonNormalizerWeb.Gettext
  import MastodonNormalizerWeb.StatusComponents

  attr :instance, :string, required: true
  attr :posts, :list, required: true
  attr :show, :integer
  attr :collapsed, :boolean, default: false

  def render(assigns) do
    author = hd(assigns.posts).account
    assigns = Map.put_new(assigns, :author, author)
    # TODO: visually collapse remainder and allow expansion
    limit = assigns[:show] || length(assigns.posts)
    assigns = Map.put(assigns, :posts, Enum.take(assigns.posts, limit))

    ~H"""
    <article
      class="status-row m-2 md:m-4 focus-within:bg-slate-50 dark:focus-within:bg-black focus-within:left-0 focus-within:ring ring-offset-4 hover:outline outline-1 outline-offset-4 outline-blue-600 outline-offset-2 relative"
      id={@id}
      phx-hook="StatusRow"
      tabindex="0"
    >
      <p>
        <h2 class="inline text-lg">
          <.account account={@author} local={"#{@instance}/@#{@author.acct}"} />
        </h2>
        <span :if={length(@posts) > 1} class="text-slate-500">
          ({ngettext("%{count} post", "%{count} posts", length(@posts))})
        </span>
      </p>
      <div
        name="collapsible-row"
        class={
          [
            "static snap-x snap-mandatory md:snap-proximity overflow-y-hidden",
            # FIXME: @focused (?) as well as pseudostyle for immediate feedback
            case @collapsed do
              true ->
                "h-48 focus-within:h-auto overflow-y-hidden"

              _ ->
                "h-auto"
            end
          ]
        }
      >
        <div class="flex flex-row space-x-4">
          <%= for post <- @posts do %>
            <.status_card post={post} instance={@instance} collapsed={@collapsed} />
          <% end %>
        </div>
        <div
          :if={@collapsed}
          class="absolute bottom-0 w-full h-2 md:h-6 bg-gradient-to-t from-white dark:from-zinc-900"
        >
          <%!-- FIXME: Naturally short rows should not have the fade style applied.
          Fade right edge too. --%>
        </div>
      </div>
    </article>
    """
  end
end
