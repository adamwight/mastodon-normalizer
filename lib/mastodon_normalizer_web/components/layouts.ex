defmodule MastodonNormalizerWeb.Layouts do
  use MastodonNormalizerWeb, :html

  embed_templates "layouts/*"
end
