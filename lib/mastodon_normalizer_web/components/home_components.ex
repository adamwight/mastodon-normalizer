defmodule MastodonNormalizerWeb.HomeComponents do
  use Phoenix.Component

  use Gettext, backend: MastodonNormalizerWeb.Gettext
  import Phoenix.HTML, only: [raw: 1]
  use MastodonNormalizerWeb, :verified_routes

  attr :last_instance, :string

  def login(assigns) do
    ~H"""
    <div class="md:m-6">
      <form method="get" action="/auth/mastodon">
        <label class="block md:inline" for="instance">{gettext("Instance")}</label>
        <input
          type="text"
          class="dark:bg-zinc-700 dark:text-white"
          name="instance"
          id="instance"
          value={@last_instance}
        />
        <input
          type="submit"
          value={gettext("Login")}
          class="m-2 md:m-6 cursor-pointer bg-blue-500 rounded-xl px-6 py-2 md:py-4 text-lg font-semibold text-zinc-900 block md:inline"
        />
      </form>
    </div>
    """
  end

  def intro(assigns) do
    ~H"""
    <p class="my-4 font-semibold text-zinc-900">
      {gettext("to normalize your Mastodon feed, giving each
      author equal space regardless of how often they post.")}
    </p>
    <p>
      {raw(
        gettext(~s(
        <span class="font-semibold">Credits</span>: Implementations by Adam Wight.
        "Check in" mode is from a suggestion by %{post}.
      ),
          post:
            ~s(<a href="https://sauropods.win/@futurebird/109971101661561998">@futurebird@sauropods.win</a>)
        )
      )}
    </p>
    <p class="mt-4">
      {raw(gettext(~s(
        <span class="font-semibold">Data protection statement</span>: After logout,
        none of your data is retained except for the host name of your
        login server.<br />This is just a prototype, there's no business model.
      )))}
    </p>
    <p class="mt-4 text-base text-zinc-600 font-semibold">
      {raw(
        gettext(~s(<a href="%{url}">Guided user tutorial</a>.),
          url: ~p"/help"
        )
      )}
    </p>
    <p class="mt-4 text-base text-zinc-600 font-semibold">
      {raw(
        gettext(~s(<a href="%{src}">Open source</a>.),
          src: "https://gitlab.com/adamwight/mastodon-normalizer"
        )
      )}
    </p>
    <p class="mt-4 text-base text-zinc-600 font-semibold">
      {raw(
        gettext(~s(<a href="%{url}">Read more about the project</a>.),
          url: "https://mw.ludd.net/wiki/Mastodon_normalizer"
        )
      )}
    </p>
    """
  end
end
