defmodule MastodonNormalizer.Repo do
  use Ecto.Repo,
    otp_app: :mastodon_normalizer,
    adapter: Ecto.Adapters.Postgres
end
