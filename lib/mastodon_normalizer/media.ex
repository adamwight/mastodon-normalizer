defmodule MastodonNormalizer.Media do
  import Ecto.Changeset
  use Ecto.Schema
  @primary_key false

  embedded_schema do
    field :description, :string
    field :id, :string
    field :preview_url, :string
    field :type, :string
    field :url, :string
  end

  def changeset(media, params \\ %{}) do
    media
    |> cast(params, ~w(description id preview_url type url)a)
  end
end
