defmodule MastodonNormalizer.Oauth2App do
  use Ecto.Schema
  import Ecto.Changeset

  schema "apps" do
    field :base_url, :string
    field :client_id, :string
    field :client_secret, :string, redact: true
    field :name, :string
    field :remote_id, :string

    timestamps()
  end

  def changeset(app, params \\ %{}) do
    app
    |> cast(params, ~w(base_url client_id client_secret name remote_id)a)
    |> validate_required(~w(base_url client_id client_secret)a)
  end
end
