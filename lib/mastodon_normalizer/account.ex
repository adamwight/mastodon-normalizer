defmodule MastodonNormalizer.Account do
  import Ecto.Changeset
  use Ecto.Schema
  @primary_key false

  embedded_schema do
    field :acct, :string
    field :display_name, :string
    field :id, :string
    field :url, :string
  end

  def changeset(account, params \\ %{}) do
    account
    |> cast(params, ~w(acct display_name id url)a)
  end
end
