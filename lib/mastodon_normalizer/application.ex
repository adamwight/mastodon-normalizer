defmodule MastodonNormalizer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Start the Telemetry supervisor
      MastodonNormalizerWeb.Telemetry,
      # Start the Ecto repository
      MastodonNormalizer.Repo,
      # Start the PubSub system
      {Phoenix.PubSub, name: MastodonNormalizer.PubSub},
      # Start Finch
      {Finch, name: MastodonNormalizer.Finch},
      # Start the Endpoint (http/https)
      MastodonNormalizerWeb.Endpoint,
      MastodonNormalizer.PromEx
      # Start a worker by calling: MastodonNormalizer.Worker.start_link(arg)
      # {MastodonNormalizer.Worker, arg}
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: MastodonNormalizer.Supervisor]
    Supervisor.start_link(children, opts)
  end

  # Tell Phoenix to update the endpoint configuration
  # whenever the application is updated.
  @impl true
  def config_change(changed, _new, removed) do
    MastodonNormalizerWeb.Endpoint.config_change(changed, removed)
    :ok
  end
end
