defmodule MastodonNormalizer.Emoji do
  def replace_shortcodes(text) do
    Regex.replace(~r":([\w-_]+):", text, fn original, shortcode ->
      Emojix.find_by_shortcode(shortcode)
      |> case do
        nil ->
          original

        emoji ->
          emoji.unicode
      end
    end)
  end
end
