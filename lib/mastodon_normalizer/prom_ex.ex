defmodule MastodonNormalizer.PromEx do
  use PromEx, otp_app: :mastodon_normalizer

  alias PromEx.Plugins

  @impl true
  def plugins do
    [
      # PromEx built in plugins
      Plugins.Application,
      Plugins.Beam,
      {Plugins.Phoenix,
       router: MastodonNormalizerWeb.Router, endpoint: MastodonNormalizerWeb.Endpoint},
      # Plugins.Ecto,
      Plugins.PhoenixLiveView,
      # Plugins.Absinthe,
      # Plugins.Broadway,

      # Add your own PromEx metrics plugins
      MastodonNormalizer.PromExPlugin.Api
    ]
  end

  @impl true
  def dashboard_assigns do
    [
      datasource_id: "prom_source",
      default_selected_interval: "30s"
    ]
  end

  @impl true
  def dashboards do
    [
      # PromEx built in Grafana dashboards
      {:prom_ex, "application.json"},
      {:prom_ex, "beam.json"}
      # {:prom_ex, "phoenix.json"},
      # {:prom_ex, "ecto.json"},
      # {:prom_ex, "phoenix_live_view.json"},
      # {:prom_ex, "absinthe.json"},
      # {:prom_ex, "broadway.json"},

      # Add your dashboard definitions here with the format: {:otp_app, "path_in_priv"}
      # {:mastodon_normalizer, "/grafana_dashboards/user_metrics.json"}
    ]
  end
end

defmodule MastodonNormalizer.PromExPlugin.Api do
  use PromEx.Plugin

  @impl true
  def event_metrics(opts) do
    otp_app = Keyword.fetch!(opts, :otp_app)
    metric_prefix = Keyword.get(opts, :metric_prefix, PromEx.metric_prefix(otp_app, :absinthe))

    Event.build(
      :mastodon_normalizer_api_metrics,
      [
        distribution(
          metric_prefix ++ [:mastodon_api, :fetch_timeline, :duration, :milliseconds],
          event_name: [:api, :fetch_timeline, :stop],
          description: "Time taken to request a timeline from a remote Mastodon API.",
          measurement: :duration,
          # tags: [:instance]
          unit: {:native, :millisecond},
          reporter_options: [
            buckets: [10, 100, 500, 1_000, 2_500, 5_000, 10_000]
          ]
        )
      ]
    )
  end
end
