defmodule MastodonNormalizer.MastodonApi do
  alias MastodonNormalizer.Oauth2App
  alias MastodonNormalizer.Repo
  alias Ueberauth.Strategy.Mastodon.API
  require Logger

  @scopes "read write:favourites"
  @timeline_limit 40

  def normalize_base_url(url) do
    url = String.trim(url)
    url = Regex.replace(~r"/(@.+)?$", url, "")

    if not String.contains?(url, "://") do
      "https://" <> url
    else
      url
    end
  end

  # TODO: should be split into API and DB
  # FIXME: callback_uri is a wart
  def get_app(instance, callback_uri) do
    Repo.get_by(Oauth2App, base_url: instance)
    |> case do
      nil ->
        Logger.info("Requesting new app credentials for #{instance}...")

        {:ok,
         %{
           status: 200,
           body:
             %{
               "client_id" => client_id,
               "client_secret" => client_secret
             } = response
         }} =
          API.app_create(instance, %{
            client_name: "mastodon-normalizer",
            redirect_uris: callback_uri,
            scopes: @scopes,
            website: "https://normalizer.webflux.us"
          })

        # FIXME: handle errors
        # TODO: looks like a changeset should do this required+optional mapping
        %Oauth2App{
          base_url: instance,
          client_id: client_id,
          client_secret: client_secret,
          name: Map.get(response, "name"),
          remote_id: Map.get(response, "id")
        }
        |> Repo.insert!()

      app ->
        app
    end
  end

  def provider_config(instance, callback_uri) do
    app = get_app(instance, callback_uri)

    {
      Ueberauth.Strategy.Mastodon,
      [
        instance: instance,
        client_id: app.client_id,
        client_secret: app.client_secret,
        scope: @scopes
      ]
    }
  end

  def revoke_token(instance, callback_uri, token) when is_binary(token) do
    app = get_app(instance, callback_uri)

    %MastodonClient.Conn{instance: instance, access_token: token}
    |> MastodonClient.post("/oauth/revoke", %{
      client_id: app.client_id,
      client_secret: app.client_secret,
      token: token
    })
  end

  def parse_status(status) do
    %MastodonNormalizer.Status{}
    |> MastodonNormalizer.Status.changeset(status)
    |> Ecto.Changeset.apply_changes()
  end

  def get_home_timeline(instance, token, max_status_id \\ "") do
    # TODO: handle failure; refresh expired token

    %MastodonClient.Conn{instance: instance, access_token: token}
    |> MastodonClient.get!("/api/v1/timelines/home",
      query: %{limit: @timeline_limit, max_id: max_status_id}
    )
    |> Map.get(:body)
    |> Enum.map(&parse_status/1)
  end

  def get_status(instance, token, status_id) do
    %MastodonClient.Conn{instance: instance, access_token: token}
    |> MastodonClient.get!("/api/v1/statuses/#{status_id}")
    |> Map.get(:body)
    |> parse_status()
  end

  def add_favorite(instance, token, post_id) do
    %{status: 200} =
      %MastodonClient.Conn{instance: instance, access_token: token}
      |> MastodonClient.post!("/api/v1/statuses/#{post_id}/favourite", %{})
  end

  def get_batch_size(), do: @timeline_limit
end
