defmodule MastodonNormalizer.StatusSanitizer do
  @moduledoc """
  A rendered status should only include tags and attributes from
  MASTODON_STRICT, minus a few.
  """
  require HtmlSanitizeEx.Scrubber.Meta
  alias HtmlSanitizeEx.Scrubber.Meta

  Meta.remove_cdata_sections_before_scrub()

  Meta.strip_comments()

  Meta.allow_tag_with_uri_attributes("a", ["href"], ["http", "https"])
  Meta.allow_tag_with_these_attributes("a", ["name", "title"])

  Meta.allow_tag_with_this_attribute_values("a", "rel", ["nofollow", "noopener", "noreferrer"])
  Meta.allow_tag_with_this_attribute_values("span", "class", ["ellipsis", "invisible"])
  Meta.allow_tag_with_these_attributes("ol", ["start", "reversed"])
  Meta.allow_tag_with_these_attributes("li", ["value"])

  # FIXME: too bad, this sets off a compiler error.
  # ~w(p br del pre blockquote code b strong u i em ul)
  # |> Enum.each(& Meta.allow_tag_with_these_attributes(&1, []))
  Meta.allow_tag_with_these_attributes("p", [])
  Meta.allow_tag_with_these_attributes("br", [])
  Meta.allow_tag_with_these_attributes("del", [])
  Meta.allow_tag_with_these_attributes("pre", [])
  Meta.allow_tag_with_these_attributes("blockquote", [])
  Meta.allow_tag_with_these_attributes("code", [])
  Meta.allow_tag_with_these_attributes("b", [])
  Meta.allow_tag_with_these_attributes("strong", [])
  Meta.allow_tag_with_these_attributes("u", [])
  Meta.allow_tag_with_these_attributes("i", [])
  Meta.allow_tag_with_these_attributes("em", [])
  Meta.allow_tag_with_these_attributes("ul", [])

  Meta.strip_everything_not_covered()
end
