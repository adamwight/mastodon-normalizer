defmodule MastodonNormalizer.Status do
  alias MastodonNormalizer.StatusSanitizer
  import Ecto.Changeset
  use Ecto.Schema
  @primary_key false

  embedded_schema do
    embeds_one :account, MastodonNormalizer.Account
    field :avatar, :string
    field :avatar_static, :string
    field :bot, :boolean
    field :content, :string
    field :discoverable, :boolean
    field :display_name, :string
    field :favourited, :boolean
    field :id, :string
    field :language, :string
    embeds_many :media_attachments, MastodonNormalizer.Media
    field :muted, :boolean
    embeds_one :reblog, MastodonNormalizer.Status
    field :sensitive, :boolean
    field :spoiler_text, :string
    field :uri, :string
    field :url, :string
    field :username, :string
    field :visibility, :string
    timestamps inserted_at: :created_at, updated_at: :edited_at
  end

  defp sanitize_html(html),
    do: HtmlSanitizeEx.Scrubber.scrub(html, StatusSanitizer)

  defp retarget_links(html) do
    Floki.parse_fragment!(html)
    |> Floki.find_and_update("a", fn {"a", attrs} -> {"a", [{"target", "_blank"} | attrs]} end)
    |> Floki.raw_html()
  end

  def changeset(status, params \\ %{}) do
    status
    |> cast(
      params,
      ~w(
        avatar avatar_static bot created_at discoverable display_name edited_at
        favourited id language muted sensitive spoiler_text uri url username visibility
      )a
    )
    |> change(%{content: params["content"] |> sanitize_html() |> retarget_links()})
    |> cast_embed(:account)
    |> cast_embed(:media_attachments)
    |> cast_embed(:reblog)
  end
end
